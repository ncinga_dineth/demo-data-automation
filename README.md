# Demo Project Data Automation

This project was generated with [Apache JMeter™](https://jmeter.apache.org) version 4.0

## Requirements

* JMeter 4.0 is compatible with Java 8 or Java 9. We highly advise you to install latest minor version of those major versions for security and performance reasons.
* JMeter is a 100% Java application and should run correctly on any system that has a compliant Java implementation.
* You will need to add mongo-java-driver (3.8.0) to path /lib. Make sure the file is a jar file, not a zip.

## Running JMeter

### run JMeter (in GUI mode by default)

Go to JMeter/bin folder and open up a terminal and give this command. (LINUX)
```
./jmeter
```

Go to JMeter/bin folder and open up a command promot and give this command. (WINDOWS)
```
jmeter
```

### run JMeter (in Non GUI mode)

Go to JMeter/bin folder and open up a terminal and give this command with the script you want to run. (LINUX)

* -n – non-GUI mode – this specifies JMeter is to run in non-GUI mode
* -t – JMX file – location of the test plan and the name of JMX file that contains the Test Plan
* -l – log file name of JTL file to log sample results to

```
./jmeter.sh -n -t my_test_plan.jmx -l log.jtl
```

### Optional
* -j – name of JMeter run log file
* -r – Run the test in the servers specified by the JMeter property “remote_hosts”
* -R – list of remote servers Run the test in the specified remote servers
* -H – proxy server hostname or ip address
* -P – proxy server port

## JMeter's Classpath

used for utility jars
```
JMETER_HOME/lib
```

used for JMeter components and plugins
```
JMETER_HOME/lib/ext
```

## Running Multiple JMeter Scripts

Taurus is used to start multiple JMeter tests at the same time. Use Taurus CLI Tool v1.13.2 for this project.

### Command-line Arguments

``` 
bzt ExhibitA.jmx ExhibitB.jmx /somefolder/someotherfolder/ExhibitC.jmx
```

## Authors

**Dineth Kariyawasam** - *Initial work*
